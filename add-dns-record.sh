#!/bin/bash

## Allows for creation of "Basic" DNS records in a Route53 hosted zone

function main() {
	record_name=$1
	record_value=$2
	selected_hosted_zone_id=$3

	[[ -z $record_name  ]] && echo "record_name is: $record_name" && exit 1
	[[ -z $record_value ]] && echo "record_value is: $record_value" && exit 1

	## set some defaults if variables haven't been overridden on script execute
	zone_name=${zone_name:-$ROUTE53_DEFAULT_HOSTED_ZONE_NAME}
	action=${action:-UPSERT}
	record_type=${record_type:-A}
	ttl=${ttl:-300}
	wait_for_sync=${wait_for_sync:-false}

	change_id=$(submit_resource_record_change_set) || exit 1
	echo "Record change submitted! Change Id: $change_id"
	if $wait_for_sync; then
		echo -n "Waiting for all Route53 DNS to be in sync..."
		until [[ $(get_change_status $change_id) == "INSYNC" ]]; do
		 	echo -n "."
		 	sleep 5
		done
		echo "!"
		echo "Your record change has now propogated."
	fi
	echo 'Thank you for using "The Cloud".'
}

function change_batch() {
	jq -c -n "{\"Changes\": [{\"Action\": \"$action\", \"ResourceRecordSet\": {\"Name\": \"$record_name\", \"Type\": \"$record_type\", \"TTL\": $ttl, \"ResourceRecords\": [{\"Value\": \"$record_value\"} ] } } ] }"
}

function get_change_status() {
	aws route53 get-change --id $1 | jq -r '.ChangeInfo.Status'
}

# function hosted_zone_id() {
#    aws route53 list-hosted-zones | jq -r ".HostedZones[] | select(.Name == \"${zone_name}\") | .Id" | cut -d'/' -f3
# }

function submit_resource_record_change_set() {
	aws route53 change-resource-record-sets --hosted-zone-id $selected_hosted_zone_id --change-batch $(change_batch) | jq -r '.ChangeInfo.Id' | cut -d'/' -f3
}

function usage() {
	echo "usage: $0 <record_name> <record_value>"
	echo ""
	echo "possible env config settings and their defaults:"
	echo "  - action=UPSERT"
	echo "  - ttl=300"
	echo "  - record_type=A"
	echo "  - wait_for_sync=-false"
	echo ""
}

main $1 $2 $3
